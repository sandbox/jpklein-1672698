What is Siteminder?
===================

The Siteminder module allows for users to authenticate to a Drupal site when the site is being run within the Siteminder environment.  Siteminder itself is a single sign-on application made by Computer Associates.  You can read more about the Siteminder system here http://www.ca.com/us/internet-access-control.aspx


How Siteminder module works
===========================

The basics:  The Siteminder module works by looking at what headers a user's Siteminder-enabled browser is sending to the Drupal website.  The Siteminder module uses one of these headers as the key identifier which maps to a user id in Drupal.  IMPORTANT: See security note below.  If the module is able to find a mathing ID in Drupal, the users is logged in to Drupal.  Otherwise, a new user is created.  Siteminder module uses other headers that are sent by the Siteminder system as values for a user's email and username.

Mapping and extensibility: The Siteminder module allows mappings to be built so that an admin user can easily map which header values should be mapped to which Drupal values when creating a new user.  The module is extensible whereby plugin modules can be written that can provide fields to which header values should be mapped.  Plugin modules are then provided the opportunity to handle the processing and storage for the fields it provides for Siteminder mapping.  Siteminder module comes with a plugin module called Siteminder Profile which allows a user profile (using the content profile module) to be created and populated based on the Siteminder mapping that is active on the site.  

Mapping and Chaos Tools: Siteminder module takes advantage of the Chaos Tools module in order to allow for Siteminder mappings to be built much like the way Views are built, whereby a mapping can be provided in code, created in the UI and stored in the database, or overridden if stored in code.  A mapping can be stored in code by creating a new file in the siteminder directory called siteminder.siteminder_default.inc.  Create a function in this file called siteminder_default_siteminder() and return an array of siteminder mapping objects.  Use the Siteminder UI to create and then export a siteminder mapping.


Developers
==========

If you are interested in writing a plugin module for the Siteminder module, there are two important hooks you module should implement.  The first is hook_siteminder_fields() which should return an array in the following format, using content profile as an example:

$fields = array(
  'name' => 'Node profile mappings',
  'fields' => array(
    'field_phone' => array(
      'title' => 'Phone number'
     ),
    'field_fax' => array(
      'title' => 'Fax number',
     ),
    'field_nickname' => array(
      'title' => 'Nickname',
     ),
  ),
);

Please refer to the Siteminder Profile implementation of this hook for an actual example.

The second hook any plugin module should implement is hook_siteminder_login_post() which contains three arguments $account, $map, and $headers in its signature.  Use this hook to do things with the fields values that your module provides, such as saving values to your plugin module's target storage, etc.  Take the keys in $map and retrieve the corresponding values in $header for any fields that your module is implementing.  $map will contain a keyed array within which is a mapping for your specific plugin module.  Again, see Siteminder Profile for an example.


Security
========

The Siteminder module should only be used on a Drupal site entirely protected by the Siteminder system.  If the site is not entirely protected by the Siteminder system, there are obvious security implications and the module should not be used under such circumstances.  Contact the maintainer of this module for any questions.


Credits
=======

This module is sponsored by Development Seed with contributions from the World Bank.  A Siteminder module was originally developed by Phase 2 Technologies.  I've taken this module and done a near-complete rewrite.  The authentication system works close to the same, but I rewrote the majority of everything else.


